/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.inherritance;

/**
 *
 * @author User
 */
public class TastAnimal {

    public static void main(String[] args) {
        Animal animal = new Animal("Ani", "white", 0);
        animal.speak();
        animal.walk();

        Dog dang = new Dog("Dang", "Black&White");
        dang.speak();
        dang.walk();
        
        Cat zero = new Cat("Zero", "Orange");
        zero.speak();
        zero.walk();
        
        Duck som = new Duck("Som", "Orange");
        som.speak();
        som.walk();
        som.fly();
        
    }
}
